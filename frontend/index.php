<?php
session_start();
if(isset($_SESSION['loggedin']) && $_SESSION['loggedin']=='true')
{
    header('Location: user_login.php');
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Login Page</title>
  <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>
<body>
  <div class="container">
    <h1 style="color:#45a049">Register System</h1>
	<h1>Login</h1>
    <form action="../backend/validate.php" method="post">
      <input type="text" placeholder="Username" name="username" required>
      <input type="password" placeholder="Password" name="password" required>
      <button type="submit">Log in</button>
    </form>
    <small style="color:red"><?php if(isset($_GET['err'])){echo $_GET['err'];}?></small>
  </div>
</body>
</html>
