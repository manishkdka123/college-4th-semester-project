<?php
require('../backend/logout.php');
session_start();
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != 'true') {
  header('Location: index.php');
  exit;
}
if (isset($_POST['logout'])) {
  logout();
}
?>
<?php
   if(isset($_GET['id']))
   {
    include("../utils/FetchData.php");
    $data=fetchStudentData($_GET['id']);    
   }
   ?>
<?php include("../assets/header.php"); ?>
<style>
  
  .centerthis {
  display: flex;
  justify-content: center;  
  height: 80vh;
  margin: auto;
  width: 30%;
  padding: 10px;
  
}

table, th, td {
  border: none;
  background-color: white;
}

td {
  text-align: left;
}

label {
  font-weight: bold;
}

input[type="text"],
input[type="number"],
input[type="date"] {
  width: 100%;
  padding: 8px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type="radio"] {
  margin-right: 5px;
}

input[type="submit"] {
  padding: 10px 20px;
  background-color: #4CAF50;
  color: white;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type="submit"]:hover {
  background-color: #45a049;
}

.details {
  vertical-align: top;
}

  
  </style>
  <h1 style="text-align: center;color:#45a049;"><?php
   if(isset($_GET['id']))
   echo "STUDENT INFORMATION UPDATE FORM";
   else
   echo "STUDENT REGISTRATION";
   ?>
   </h1>
   <?php
   if(!isset($_GET['id']))
   echo "<form action='../backend/addstudentbackend.php' method='post'>";
   else
   echo "<form action='../backend/update_student.php' method='post'>";
   ?>

<form action="../backend/addstudentbackend.php" method="post">
  

<div class="centerthis">
    
  <table>
    <tr>
      <td>
        <label for="1">First Name </label>
</td>
      <td><input type="text" id="1" name="fname" placeholder="Enter your First name" required value=<?php if(isset($data))echo $data['First Name'];else echo ""; ?>></td>
</tr>
    <tr>
      <td>
        <label for="2">Middle Name</label>
      </td>
      <td>
        <input type="text" id="2" name="mname" placeholder="Enter your Middle name" value=<?php if(isset($data))echo $data['Middle Name'];else echo ""; ?>>
      </td>
    <tr>
      <td>
        <label for="3">Last Name </label>
      </td>
      <td>

        <input type="text" id="3" name="lname" placeholder="Enter your last name" required value=<?php if(isset($data))echo $data['Last Name'];else echo ""; ?>>
      </td>
    </tr>
    <tr>
      <td>
        <label>Gender</label><br>
      </td>
      <td>
        <input type="radio" id="m" name="gender" value="Male" <?php if(isset($data) && $data['Gender']=="Male") echo "checked";?> >
        <label for="m">Male</label><br>
        <input type="radio" id="f" name="gender" value="Female" <?php if(isset($data) && $data['Gender']=="Female") echo "checked";?>>
        <label for="f">Female</label><br>
        <input type="radio" id="no" name="gender" value="" <?php if(isset($data) && $data['Gender']=="") echo "checked";?>>
        <label for="no">Prefer not to say</label>
      </td>
</tr>

      <td>
        <label>Date of Birth</label>
      </td>
      <td>
        <input type="date" name="dob" required value=<?php if(isset($data))echo $data['DOB'];else echo ""; ?>>
      </td>
    </tr>
    <tr>
      <td>
        <label class="details">Address </label>
      </td>
      <td>

        <input type="text" name="address" value=<?php if(isset($data))echo $data['Address'];else echo ""; ?>>
      </td>
    </tr>
    <tr>
      <td>
        <label class="details">Phone Number </label>
      </td>
      <td>
        <input type="number" name="phone_number" value=<?php if(isset($data))echo $data['Phone'];else echo ""; ?>>
      </td>
    </tr>
  <tr><td colspan="2">
    <input type="number" name="id" hidden value=<?php if(isset($data))echo $data['id'];else echo ""; ?>>
    <input type="submit" value=<?php
   if(isset($_GET['id']))
   echo "UPDATE";
   else
   echo "ADD";
   ?> name="send">
</td>
</tr>
</table>
</div>
</form>

<?php include("../assets/footer.php"); ?>