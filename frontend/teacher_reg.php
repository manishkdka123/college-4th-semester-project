<?php
require('../backend/logout.php');
session_start();
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != 'true') {
  header('Location: index.php');
  exit;
}
if (isset($_POST['logout'])) {
  logout();
}
?>
<?php
   if(isset($_GET['tid']))
   {
    include("../utils/FetchData.php");
    $data=fetchTeacherData($_GET['tid']);    
   }
   ?>
<?php include("../assets/header.php"); ?>
<style>
  .centerthis {
    display: flex;
    justify-content: center;
    height: 80vh;
    margin: auto;
    width: 30%;
    padding: 10px;

  }

  table,
  th,
  td {
    border: none;
    background-color: white;
  }

  td {
    text-align: left;
  }

  label {
    font-weight: bold;
  }

  input[type="text"],
  input[type="number"],
  input[type="date"],
  input[type="password"],
  select{
    width: 100%;
    padding: 8px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type="radio"] {
    margin-right: 5px;
  }

  input[type="submit"] {
    padding: 10px 20px;
    background-color: #4CAF50;
    color: white;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type="submit"]:hover {
    background-color: #45a049;
  }

  .details {
    vertical-align: top;
  }
</style>
<h1 style="text-align:center;color:#45a049;"><?php
   if(isset($_GET['tid']))
   echo "TEACHER INFORMATION UPDATE FORM";
   else
   echo "TEACHER REGISTRATION";
   ?></h1>
<?php
   if(!isset($_GET['tid']))
   echo "<form action='../backend/addteacherbackend.php' method='post'>";
   else
   echo "<form action='../backend/update_teacher.php' method='post'>";
   ?>
  <div class='centerthis'>
  <table>
    <tr>
      <td>
        <label for="1">First Name </label>
      </td>
      <td>

        <input type="text" id="1" name="fname" placeholder="Enter your First name" required value=<?php if(isset($data))echo $data['First Name'];else echo ""; ?>>
      <td>
    </tr>
    <tr>
      <td>
        <label for="2">Middle Name</label>
      </td>
      <td>
        <input type="text" id="2" name="mname" placeholder="Enter your Middle name" value=<?php if(isset($data))echo $data['Middle Name'];else echo ""; ?>>
      </td>
    </tr>
    <tr>
      <td>
        <label for="3" class="details">Last Name </label>
      </td>
      <td>
        <input type="text" id="3" name="lname" placeholder="Enter your last name" required value=<?php if(isset($data))echo $data['Last Name'];else echo ""; ?>>
      </td>
    </tr>

    <?php if(!isset($_GET['tid'])){
echo <<<END
<tr>
      <td>


        <label>Username</label>
      </td>
      <td>
        <input type="text" name="username" placeholder="Pick a username" required>
      </td>

    </tr>

    <tr>

      <td> <label>Password</label></td>

      <td><input type="password" name="password"  required></td>
    </tr>


    <tr>
      <td>
        <label>Confirm Password</label>
      </td>
      <td>
        <input type="password" name="cpassword" placeholder="must be same as above field" required>
      </td>
    </tr>
END;
    }  
    ?>  

    <tr>
      <td>
        <label for="4" >Subject</label>
      </td>
      <td>
        <select id="4" name="subject" required>
          <option value="" <?php if(isset($data) && $data['Subject']=="") echo "selected";?>>Select</option>
          <option value="Mathematics" <?php if(isset($data) && $data['Subject']=="Mathematics") echo "selected";?> >Mathematics</option>
          <option value="Science" <?php if(isset($data) && $data['Subject']=="Science") echo "selected";?>>Science</option>
          <option value="English" <?php if(isset($data) && $data['Subject']=="English") echo "selected";?>>English</option>
          <option value="Social" <?php if(isset($data) && $data['Subject']=="Social") echo "selected";?>>Social Studies</option>
          <option value="General Knowledge" <?php if(isset($data) && $data['General Knowledge']=="Vocational") echo "selected";?>>General Knowledge</option>
          <option value="Computer" <?php if(isset($data) && $data['Subject']=="Computer") echo "selected";?>>Computer Science</option>
          <option value="Nepali" <?php if(isset($data) && $data['Subject']=="Nepali") echo "selected";?>>Nepali</option>
          <option value="Vocational" <?php if(isset($data) && $data['Subject']=="Vocational") echo "selected";?>>Vocational</option>
        </select>
      </td>
    </tr>

    <tr>
      <td>
        <label>Gender</label><br>
      </td>
      <td>
        <input type="radio" id="m" name="gender" value="Male" value="Male" <?php if(isset($data) && $data['Gender']=="Male") echo "checked";?>>
        <label for="m">Male</label><br>
        <input type="radio" id="f" name="gender" value="Female"  <?php if(isset($data) && $data['Gender']=="Female") echo "checked";?>>
        <label for="f">Female</label><br>
        <input type="radio" id="no" name="gender" value="" <?php if(isset($data) && $data['Gender']=="") echo "checked";?>>
        <label for="no">Prefer not to say</label>
      </td>
    </tr>
    <tr>
      <td>
        <label>Address </label>
      </td>
      <td>

        <input type="text" name="address" value=<?php if(isset($data))echo $data['Address'];else echo ""; ?>>
      </td>
    </tr>
    <tr>
      <td>
        <label>Phone Number </label>
      </td>
      <td>
        <input type="number" name="phone_number" value=<?php if(isset($data))echo $data['Phone'];else echo ""; ?>>
      </td>
    </tr>
    <tr>
      <td colspan="2">
      <input type="number" name="tid" hidden value=<?php if(isset($data))echo $data['tid'];else echo ""; ?>>
    <input type="submit" value=<?php
   if(isset($_GET['tid']))
   echo "UPDATE";
   else
   echo "ADD";
   ?> name="send">
      </td>
    </tr>
  </table>
  </div>
</form>

<?php include("../assets/footer.php");