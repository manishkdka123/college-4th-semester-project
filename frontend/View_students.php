<?php
require('../backend/logout.php');
session_start();
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != 'true') {
    header('Location: index.php');
}
if (isset($_POST['logout'])) {
    logout();
}
?>

<?php include("../assets/header.php"); ?>


<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Date of Birth</th>
            <th>Address</th>
            <th>Phone number</th>
            <th>Update</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <?php
        require('../database/dbcon.php');
        $sql = "SELECT * FROM student";
        $result = mysqli_query($conn, $sql);
        if (!$result) {
            die("Query failed");
        } else {
            while ($row = mysqli_fetch_assoc($result)) {
                ?>
                <tr>
                    <td>
                        <?php echo $row['id']; ?>
                    </td>
                    <td>

                        <?php $name=$row['First Name']." ".$row['Middle Name']." ".$row['Last Name'];echo $name; ?>
                    </td>
                    <td>
                        <?php echo $row['Gender']; ?>
                    </td>
                    <td>
                        <?php echo $row['DOB']; ?>
                    </td>
                    <td>
                        <?php echo $row['Address']; ?>
                    </td>
                    <td>
                        <?php echo $row['Phone']; ?>
                    </td>
                    <td>
                        <a class="btn two" href="../frontend/student_reg.php?id=<?php echo $row['id']; ?>">
                            <i class="fas fa-pen" style="font-size:24px;"></i>
                     </a>

                    </td>
                    <td>
                        <form action='../backend/delete_row_student.php' method='get'>
                            <input type='number' hidden value="<?php echo $row['id']; ?>" name='id'>
                            <button type='submit' class='btn one'>
                                <i class="fas fa-trash" style="font-size:24px;"></i>

                            </button>
            </form>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>



<?php include("../assets/footer.php"); ?>